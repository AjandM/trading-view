const express = require('express')
const fs = require('fs')
const request = require('request')
const cheerio = require('cheerio')
const app = express()
const mongoose = require('mongoose')
const jsdom = require("jsdom")
const htmlToText = require('html-to-text');

mongoose.connect('mongodb://localhost/trading-view');

const Idea = mongoose.model('Idea', {
    link: String,
    date: Date
})

const Updates = mongoose.model('Updates', {
    text: String,
    date: Date,
    image: String,
    timestamp: Number,
    idea: String
})

const sendMessageToTelegram = require('./telegram')
const sendMessageToTwitter = require('./twitter')


const userUrl = 'https://www.tradingview.com/u/ajand/'


const findIdeasOfPage = function (url, callback) {
    console.log('finding Ideas of page')
    request(url, function (err, response, html) {
        if (!err) {
            var $ = cheerio.load(html);
            var dateArrays = $('.tv-widget-idea__time').toArray()
            console.log('now here')
            var i = -1
            callback($('.tv-widget-idea__title').toArray().map(item => {
                i++
                return {
                    link: item.attribs.href,
                    date: new Date(parseInt(dateArrays[i].attribs['data-timestamp']) * 1000)
                }
            }))
        }
    })
}

const findPagesNumber = function (url, callback) {
    console.log('finding pages number')

    request(url, function (err, response, html) {
        if (!err) {
            var $ = cheerio.load(html);
            console.log('page numbers!')
            if ($('.tv-load-more__page').length) {
                var loadMores = $('.tv-load-more__page').toArray()
                const pageCounts = loadMores[loadMores.length - 1].attribs['data-page']
                callback(pageCounts)
            } else {
                callback(0)
            }
        }
    })
}

const findAllIdeas = function (url, callback) {
    console.log('Finding all ideas ...')

    var wholeLinks = []

    findIdeasOfPage(url, (links) => {
        wholeLinks = [...links]
        findPagesNumber(url, (pageCounts) => {
            if (pageCounts) {
                for (let i = 2; i <= pageCounts; i++) {
                    findIdeasOfPage(`${url}page-${i}`, (newLinks) => {
                        wholeLinks = [...wholeLinks, ...newLinks]
                        console.log(wholeLinks.length)
                        console.log(pageCounts)
                        if (wholeLinks.length > (pageCounts - 1) * 18) {
                            callback(wholeLinks)
                        }
                    })
                }
            } else {
                callback(wholeLinks)
            }
        })
    })
}

const findsUpdateOfIdea = function (url, callback) {
    console.log('finding updates of ideas')


    request(url, function (err, response, html) {
        if (!err) {
            var $ = cheerio.load(html);
            console.log(url)
            if ($('.tv-chart-updates__update-time').length) {
                console.log('error from here?')
                var dateArrays = $('.tv-chart-updates__update-time').toArray()
                console.log('updates of idea')
                if (dateArrays.length) {
                    var i = -1
                    callback($('.tv-chart-updates__body-wrap').toArray().map(item => {
                        const cheerioItem = cheerio.load(item)
                        var image = cheerioItem('img').length ? cheerioItem('img')['0'].attribs['src'] : null
                        i++
                        return {
                            text: htmlToText.fromString(cheerioItem.html(), {
                                wordwrap: null
                            }),
                            image,
                            date: new Date(parseInt(dateArrays[i].attribs['data-timestamp']) * 1000),
                            timestamp: parseInt(dateArrays[i].attribs['data-timestamp'])
                        }
                    }))
                }
            } else {
                callback([{
                    text: htmlToText.fromString($('.tv-chart-view__description').html(), {
                        wordwrap: null
                    }),
                    image: null,
                    date: new Date(parseInt($('.tv-chart-view__title-time')[0].attribs['data-timestamp'] * 1000)),
                    timestamp: parseInt($('.tv-chart-view__title-time')[0].attribs['data-timestamp'])
                }])
            }

        }
    })
}

const saveUpdate = function ({ text, image, date, timestamp }, idea, callback) {
    console.log('saving updates of ideas')
    console.log(timestamp)
    Updates.findOne({ timestamp }, (err, res) => {
        console.log('now here')
        if (err) return err
        if (!res) {
            console.log(text)
            console.log(timestamp)
            const newUpdate = new Updates({ text, image, date, timestamp, idea })
            newUpdate.save().then(() => Updates.findOne({ timestamp }, (err, res) => {
                if (err) return err
                callback(res)
            }))
        }
    })
}

const initialData = function (url, callback) {
    //console.log('Making initial datas ...')
    findAllIdeas(url, (allIdeas) => {
        allIdeas.map((idea) => {
            //console.log(idea)
            Idea.findOne({ link: idea.link }, (err, res) => {
                if (err) return err
                if (!res) {
                    const newIdea = new Idea({ link: idea.link, date: idea.date })
                    newIdea.save().then(() => {
                        Idea.findOne({ link: idea.link }, (err, res) => {
                            if (err) return err
                            if (res) {
                                findsUpdateOfIdea(`https://www.tradingview.com${idea.link}`, (updates) => {
                                    updates.forEach((update) => {
                                        saveUpdate(update, res._id, (update) => {
                                            console.log(update)
                                        })
                                    })
                                })
                            }
                        })
                    })
                }
            })
        })
    })
}

/*setInterval(() => {
    
}, 60000)*/

Idea.findOne({}, (err, result) => {
    if (!result) {
        initialData(userUrl)
    }
})

app.get('/', function (req, res) {
    Idea.find({}, null, { sort: { date: -1 } }, (err, ideas) => {
        if (err) return err
        res.render('index', { ideas })
    })
})


app.get('/updates/:idea', function (req, res) {
    Idea.findOne({ _id: req.params.idea }, (err, idea) => {
        if (err) return res.send(err)
        findsUpdateOfIdea(`https://www.tradingview.com${idea.link}`, (updates) => {
            updates.forEach((update) => {
                console.log('------')
                console.log(update)
                console.log('------')
                saveUpdate(update, idea._id, (update) => {
                    sendMessageToTelegram(update.text, `https://www.tradingview.com${idea.link}`)
                    sendMessageToTwitter(update.text, `https://www.tradingview.com${idea.link}`)
                })
            })
        })
        return res.redirect('/')
    })
})

app.get('/idea', function (req, res) {
    findAllIdeas(userUrl, (allIdeas) => {
        allIdeas.map((idea) => {
            Idea.findOne({ link: idea.link }, (err, res) => {
                if (err) return err
                if (!res) {
                    const newIdea = new Idea({ link: idea.link, date: idea.date })
                    newIdea.save().then(() => {
                        Idea.findOne({ link: idea.link }, (err, idea) => {
                            if (err) return err
                            if (idea) {
                                console.log(idea)
                                findsUpdateOfIdea(`https://www.tradingview.com${idea.link}`, (updates) => {
                                    updates.forEach((update) => {
                                        saveUpdate(update, idea._id, (update) => {
                                            sendMessageToTelegram(update.text, `https://www.tradingview.com${idea.link}`)
                                            sendMessageToTwitter(update.text, `https://www.tradingview.com${idea.link}`)
                                        })
                                    })
                                })
                            }
                        })
                    })
                }
            })
        })
    })

    res.redirect('/')
})

app.listen('8081')
app.set('view engine', 'pug')
console.log('Magics gonna happen in port 8081')

exports = module.exports = app